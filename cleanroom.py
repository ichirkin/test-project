import csv
import os
import random


file_path = os.environ['DATA_DIR'] + '/' + os.environ['DATASET_PATH']
result_path = os.environ['RESULT_DIR'] + '/' + os.environ['RESULT_PATH']


res = []
# open .tsv file
with open(file_path) as file:
    tsv_file = csv.DictReader(file, delimiter="\t")
    line_count = 0

    # parse data line by line
    for line in tsv_file:
        header_original = line.keys()
        # print(header_original)
        line_count = line_count + 1
        row = {}
        row['client'] = line['client']
        rnd_int = random.randint(0, 2)
        if rnd_int == 0:
            row['score'] = 0
        elif rnd_int == 1:
            row['score'] = 1
        else: 
            row['score'] = 0.555
        res.append(row)
        print("lines_processed: " + str(line_count))
        print("----------")

# write result data
header = res[0].keys()

with open(result_path, 'wt') as output_file:
    dict_writer = csv.DictWriter(output_file, header, delimiter='\t')
    dict_writer.writeheader()
    dict_writer.writerows(res)

print('1: ', file_path)
print('2:', result_path)

